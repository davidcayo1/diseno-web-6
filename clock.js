const updateTime = () => {

  let date = new Date(),
      hours = date.getHours(),
      am_pm,
      minutes = date.getMinutes(),
      seconds = date.getSeconds(),
      dayWeek = date.getDay(),
      day = date.getDate(),
      month = date.getMonth(),
      year = date.getFullYear();

    
   let d = document,
     pHours = d.getElementById("hours"),
     pAM_PM = d.getElementById("am-pm"),
     pMinutes = d.getElementById("minutes"),
     pSeconds = d.getElementById("seconds"),
     pDayWeek = d.getElementById("dayWeek"),
     pDay = d.getElementById("day"),
     pMonth = d.getElementById("month"),
     pYear = d.getElementById("year");


  /* FECHA */
   let week = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    pDayWeek.textContent = week[dayWeek]; // obtiene el valor de del arreglo por su número de posición

    pDay.textContent = day;

    let months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    pMonth.textContent = months[month];

    pYear.textContent = year;


    /* HORAS */

    // convertimos el reloj de 24 horas a 12 horas
    if (hours >= 12) {
      hours = hours - 12;
      am_pm = 'PM';
    
    } else {
      am_pm = 'AM';
    }

    // convertimos las 0 horas en las 12 de la mañana
    if (hours === 0) {
      hours = 12;
    }

    pHours.textContent = hours;
    pAM_PM.textContent = am_pm;

    if (minutes < 10) {
      minutes = '0' + minutes;
    }

    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    pMinutes.textContent = minutes;
    pSeconds.textContent = seconds;

};

// updateTime();

// Para actualizar la hora
let interval = setInterval(() => {
  updateTime();
}, 1000);